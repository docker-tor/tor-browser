#!/bin/sh

set -e

# Copyright 2016, Timothy Redaelli <timothy.redaelli@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.


if ! docker system info >/dev/null 2>&1 && [ "$(id -u)" != 0 ]; then
	docker='sudo docker'
else
	docker='docker'
fi

check_packages() {
	$docker --version >/dev/null || return
	xpra --version >/dev/null || return
}

find_docker_ip() {
	ID='' IP='' i=0
	while [ -z "$IP" ]; do
		if [ $i -gt 10 ]; then
			echo "Fatal Error" >&2
			exit 1
		fi
		i=$((i + 1))
		ID=$($docker ps -q -f "name=$1" 2>/dev/null) || continue
		IP=$($docker inspect -f '{{ .NetworkSettings.Networks.docker_tor_browser_internal.IPAddress }}' "$ID" 2>/dev/null) || continue
		sleep 1
	done
	echo "$IP"
}

# Cleanup
cleanup() {
	echo "Cleaning up..."
	$docker ps -aq -f "name=docker_tor_browser" | xargs -r $docker rm -f >/dev/null
	$docker network rm docker_tor_browser_internal docker_tor_browser_external >/dev/null 2>&1 || :
	if [ -n "$keydir" ]; then
		rm -rf "$keydir"
	fi
}

if ! check_packages; then
	echo "You need to install sudo, docker and xpra before starting this script" >&2
	exit 1
fi

trap 'cleanup' EXIT
trap 'cleanup; trap - INT EXIT; kill -INT $$' INT
trap 'cleanup; trap - TERM EXIT; kill -TERM $$' TERM

cleanup

# Waiting for cleanup to complete
echo "Waiting for cleanup to complete..."
while [ -n "$($docker ps -aq -f "name=docker_tor_browser")" ]; do
	sleep 1
done

if [ "$1" = "build" ]; then
	# Always (to import security fixes) build images and create containers
	$docker build --pull=true -t registry.gitlab.com/docker-tor/tor-node "https://gitlab.com/docker-tor/tor-node.git"
	$docker build --pull=true -t registry.gitlab.com/docker-tor/tor-browser "Dockerfile"
else
	# Use pre-built images
	$docker pull registry.gitlab.com/docker-tor/tor-node
	$docker pull registry.gitlab.com/docker-tor/tor-browser
fi

# Create networks
echo "Creating docker networks..."
$docker network create --internal docker_tor_browser_internal >/dev/null
$docker network create docker_tor_browser_external >/dev/null

# Create temporary SSH key
echo "Creating SSH key"
keydir=$(mktemp -d)
ssh-keygen -q -N '' -f "$keydir"/id_rsa

# Create tor_node containers
echo "Creating docker tor_node container..."
$docker create --net docker_tor_browser_internal --name docker_tor_browser_tor_node registry.gitlab.com/docker-tor/tor-node >/dev/null

# Attach networks to tor_node cointainer
$docker network connect docker_tor_browser_external docker_tor_browser_tor_node

# Start containers
echo "Starting tor_node container..."
$docker start docker_tor_browser_tor_node >/dev/null

echo "Waiting until tor-node fully starts..."
until $docker exec docker_tor_browser_tor_node curl --socks5 "127.0.0.1:9050" --socks5-hostname "127.0.0.1:9050" -m 5 -sI https://check.torproject.org/ >/dev/null; do
	sleep 1
done

echo "Starting tor_browser container..."
$docker create --net docker_tor_browser_internal --name docker_tor_browser_tor_browser --env UID="$(id -u)" --env GID="$(id -g)" --env=TOR_NODE_IP="$(find_docker_ip docker_tor_browser_tor_node)" --volume="$PWD/.tor-browser:/home/anon/.tor-browser:Z" --volume="$keydir/id_rsa.pub:/home/anon/.ssh/authorized_keys:Z" registry.gitlab.com/docker-tor/tor-browser >/dev/null
$docker start docker_tor_browser_tor_browser >/dev/null

echo "Finding docker-tor-browser IP address..."
IP=$(find_docker_ip docker_tor_browser_tor_browser)

echo "Connecting to docker-tor-browser via SSH and launch tor-browser"
xpra start --encoding=rgb --exit-with-children=yes --ssh="ssh -i $keydir/id_rsa -oUserKnownHostsFile=/dev/null -oConnectionAttempts=100 -oConnectTimeout=60 -oStrictHostKeyChecking=no -oCheckHostIP=no" ssh://"anon@$IP" --start-child=/usr/local/bin/tor-browser.sh
