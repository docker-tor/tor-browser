#!/bin/bash

# Copyright 2016, Timothy Redaelli <timothy.redaelli@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.

set -e

# Fingerprint from https://www.torproject.org/docs/verifying-signatures.html.en
gpg_fingerprint="EF6E 286D DA85 EA2A 4BA7  DE68 4E2C 6E87 9329 8290"

# Fetch last (stable) version of Tor Browser
echo "Fetching last version of Tor Browser (please wait)..."
{ IFS= read -r last_tb_version ; IFS= read -r last_tb_binary ; IFS= read -r last_tb_sig ; } < \
	<(torsocks wget --retry-connrefused -qO- \
		"https://dist.torproject.org/torbrowser/update_2/release/downloads.json" | \
		jshon -e version -u -p -e downloads -e linux64 -e en-US -e binary -u -p -e sig -u)

if [[ -z "$last_tb_version" || -z "$last_tb_binary" || -z "$last_tb_sig" ]]; then
	echo "Error while checking for last Tor Browser Version" >&2
	if [[ -f ".tor-browser/Browser/TorBrowser/Docs/ChangeLog.txt" ]]; then
		echo "And no Tor Browser installed" >&2
		echo "Please try again." >&2
		exit 1
	fi
else
	echo "Last Tor Browser Version: $last_tb_version"
fi

# Get installed version of Tor Browser
[[ -f ".tor-browser/Browser/TorBrowser/Docs/ChangeLog.txt" ]] && \
	installed_tb_version=$(sed -n '0,/^Tor Browser\s*/s/^Tor Browser\s*\(\S*\).*/\1/p' \
		".tor-browser/Browser/TorBrowser/Docs/ChangeLog.txt") || \
	installed_tb_version=

[[ -n "$installed_tb_version" ]] && \
	echo "Installed Tor Browser Version: $installed_tb_version"

# Install Tor Browser if not installed (upgrades are managed from inside Tor Browser itself)
if [ -z "$installed_tb_version" ]; then
	echo "Installing Tor Browser..."
	echo "Downloading Tor Browser GPG key with fingerprint $gpg_fingerprint"
	torsocks gpg1 --keyserver hkp://keyserver.ubuntu.com:80 --recv "$gpg_fingerprint"
	echo "Downloading Tor Browser $last_tb_version"
	torsocks wget --retry-connrefused -O /home/anon/tor.tar.xz "$last_tb_binary"
	echo "Download Tor Browser Signature $last_tb_version"
	torsocks wget --retry-connrefused -O /home/anon/tor.tar.xz.asc "$last_tb_sig"
	gpg1 --verify /home/anon/tor.tar.xz.asc
	tar --strip-components=1 -xJf /home/anon/tor.tar.xz -C /home/anon/.tor-browser
fi

# Start Tor Browser
# Partially taked from https://git.io/vr9LJ
# Part of Whonix.

## Deactivate tor-launcher,
## a Vidalia replacement as browser extension,
## to prevent running Tor over Tor.
## https://trac.torproject.org/projects/tor/ticket/6009
## https://gitweb.torproject.org/tor-launcher.git
export TOR_SKIP_LAUNCH=1

## tor-launcher is deactivated above but the following is required to avoid
## "Firefox is offline" messages in Tor Browser 10.5a17 and above.
## https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27476
## https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40455
## https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40433
## https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34345
export TOR_USE_LEGACY_LAUNCHER=1

## environment variable to skip TorButton control port verification
## https://trac.torproject.org/projects/tor/ticket/13079
export TOR_SKIP_CONTROLPORTTEST=1

## Environment variable to disable the "TorButton" ->
## "Open Network Settings..." menu item. It is not useful and confusing to have
## on a workstation, because Tor must be configured on the gateway, which is
## for security reasons forbidden from the gateway.
## https://trac.torproject.org/projects/tor/ticket/14100
export TOR_NO_DISPLAY_NETWORK_SETTINGS=1

/home/anon/.tor-browser/Browser/start-tor-browser
