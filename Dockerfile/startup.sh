#!/bin/sh

# Copyright 2016, Timothy Redaelli <timothy.redaelli@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.

set -e

if [ ${UID:-0} -eq 0 -o ${GID:-0} -eq 0 ]; then
	echo "Are you kidding me?" >&2
	exit 1
fi

# Remove old anon user
sed -i '/^anon:/d' /etc/passwd /etc/shadow /etc/group

# (Re)Add anon group and user
groupadd -f -g "$GID" anon
useradd -d /home/anon -s /bin/bash -u "$UID" -g "$GID" anon
mkdir -p /run/user/"$UID"
touch /home/anon/.Xauthority
chown -R anon:anon /home/anon /run/user/"$UID"

# Start internet redirection server
printf "127.0.0.1\t%s\t$TOR_NODE_IP\t%s\n" \
       9050 9050 \
       9150 9050 \
       9051 9052 \
       9151 9052  \
       >> /etc/rinetd.conf
gosu nobody /usr/sbin/rinetd

# Generate SSH host keys and start SSH server
dpkg-reconfigure openssh-server
/usr/sbin/sshd -D -o UseDNS=no
